var $ = jQuery.noConflict();
$(document).ready(function () {
  $('.table').DataTable({
    "bSort": false,
  });

  // $('.selectpicker').selectpicker();
  $('.js-example-basic-multiple').select2();

  $('#datepicker').datepicker({
    uiLibrary: 'bootstrap4',
    format: 'yyyy-mm-dd'
  });
  $('#timepicker').timepicker({
    uiLibrary: 'bootstrap4',
    format: 'HH:MM'
  });

    var action =  $('#content');
    var parent  =  action.data('parent');
    var current =  action.data('current');
  
  $('body').find('ul.'+parent).addClass('active');
  $('body').find('li.'+current).addClass('active');

  $('body').on('click', '.dropdown-icon ',function(){
    console.log('asd')
    $(this).toggleClass('transform');
  })

  $('.collapse').find('li.active').parent().addClass('show');
  $('.collapse').find('li.active').parent().next().addClass('transform');
  

    $('#sidebarCollapse').on('click', function () {
        $('#sidebar, #content').toggleClass('active');
        $('.collapse.in').toggleClass('in');
        $('a[aria-expanded=true]').attr('aria-expanded', 'false');
    });

     $('body').on('click' , '.control-btn' , function() {
         $('.control-side-bar').toggleClass('active')
     })



     $('#upload-image').change( function(){
        var input = this
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function(e) {
                $('.placeholder').attr('src', e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        }
     })
 

     $('#summernote').summernote({
      placeholder: 'Description',
      tabsize: 2,
      height: 150
    });

     $('body').on('click' ,'[data-do="show-pass"]' , function(){

        var toggle =  $(this).parent().prev('input');
        if( toggle.attr('type') == 'password' ){
            toggle.attr('type' , 'text')
        }else {
            toggle.attr('type' , 'password')
        }
     
     })



     var date = new Date();
     var d = date.getDate(),
         m = date.getMonth(),
         y = date.getFullYear();
     $('#calendar').fullCalendar({
        // droppable: true, 
        // editable: true,
        selectable: true,
    //    aspectRatio: 1.96,
       header: {
         left: 'prev,next today',
         center: 'title',
         right: 'month,agendaWeek,agendaDay'
       },
       buttonText: {
         today: 'today',
         month: 'month',
         week: 'week',
         day: 'day',
       },
       //Random default events
       eventClick: function(calEvent, jsEvent, view) {
        // console.log(calEvent,jsEvent,view)
        swal({
          title: "Remove event?",
          text: "You are able to recreate this event",
          type: "warning",
          confirmButtonClass: "btn-danger",
          confirmButtonText: "Yes, delete it!",
          showCancelButton: true,
          closeOnConfirm: false,
          showLoaderOnConfirm: true
        }, function () {

          $.request('onDelete', {   
            type: 'POST', 
            data: { id: calEvent.id },
            dataType: 'json',
            success: function(response) {
              // console.log(response)
              $('#calendar').fullCalendar('removeEvents', calEvent.id);
              swal.close()
            }
          })
          // setTimeout(function () {
          //   $('#calendar').fullCalendar('removeEvents', calEvent.id);
          //   swal.close()
          // }, 2000);


        });
       
      },
      eventDrop: function( event, delta, revertFunc, jsEvent, ui, view ){
        // console.log(event.start.format(), delta, revertFunc, jsEvent, ui, view)
      },
      select: function(startDate, endDate) {
       //console.log(selectedDay,selectedMonth,selectedYear , endDate.format());
       $('.modal').modal('show');
       $('input[name=start_date]').val(startDate.format());
       $('input[name=end_date]').val(endDate.format());

      },
      eventSources: [
        {
          events: function(startDate, endDate, timezone, callback) {


            $.request('onLoadEvents', {    
              dataType: 'json',
              success: function(response) {
                // console.log(response)
                var events = [];
                $(response.events).each(function(){
                  events.push({
                    id: $(this).attr('id'),
                    title: $(this).attr('title'),
                    start:  $(this).attr('start'),
                    end:  $(this).attr('end'),
                    
                   })
                })
                callback(events);
    
              }
            })
    
          },
          // color: 'yellow',   // an option!
          textColor: 'white' // an option!
        }
      ]
      
       
     });

    //  var input = (value,index) => {

    //     var html 
    //     html = `<div class="input-group">
    //               <span class="input-group-btn">
    //                   <button type="button" class="btn btn-default btn-number rounded-0"  data-type="minus" data-field="quant[1]">
    //                     <i class="fas fa-minus"></i>
    //                   </button>
    //               </span>
    //               <input type="text" name="LinenUsage[dirty][`+index+`][linen_item]" class="form-control input-number" value="`+value+`" min="1" max="9999">
    //               <span class="input-group-btn">
    //                   <button type="button" class="btn btn-default btn-number rounded-0" data-type="plus" data-field="quant[1]">
    //                   <i class="fas fa-plus"></i>
    //                   </button>
    //               </span>
    //           </div>`;

    //     return html;
    //  }

     $('.changetype').on('change', function() {
      $('.html-non').toggleClass('d-none');
      $('.guest').toggleClass('d-none');
      
     }) 

    $('body').on('click','.btn-number', function(e){
      e.preventDefault();
      type      = $(this).attr('data-type');

        if(type == 'minus') {
          var currentVal = parseInt($(this).parent().next().val());  
          $(".btn-number[data-type='minus'][data-field='"+name+"']").removeAttr('disabled')
            if(currentVal >  $(this).parent().next().attr('min')) {
               $(this).parent().next().val(currentVal - 1).change();
            } 
            if(parseInt( $(this).parent().next().val()) ==  $(this).parent().next().attr('min')) {
                $(this).attr('disabled', true);
            }

        } else if(type == 'plus') {
          var currentVal = parseInt($(this).parent().prev().val());
          $(".btn-number[data-type='plus'][data-field='"+name+"']").removeAttr('disabled')
            if(currentVal <  $(this).parent().prev().attr('max')) {
              $(this).parent().prev().val(currentVal + 1).change();
            }
            if(parseInt( $(this).parent().prev().val()) ==  $(this).parent().prev().attr('max')) {
                $(this).attr('disabled', true);
            }

        }

  });


  $('body').on('focusin','.input-number', function(){
     $(this).data('oldValue', $(this).val());
  });  

  $('body').on('change','.input-number',function() {
      
      minValue =  parseInt($(this).attr('min'));
      maxValue =  parseInt($(this).attr('max'));
      valueCurrent = parseInt($(this).val());
      
      name = $(this).attr('name');
      if(valueCurrent >= minValue) {
          $(".btn-number[data-type='minus'][data-field='"+name+"']").removeAttr('disabled')
      } else {
          // alert('Sorry, the minimum value was reached');
          $(this).val($(this).data('oldValue'));
      }
      if(valueCurrent <= maxValue) {
          $(".btn-number[data-type='plus'][data-field='"+name+"']").removeAttr('disabled')
      } else {
          // alert('Sorry, the maximum value was reached');
          $(this).val($(this).data('oldValue'));
      }
      
      
  });

  $('body').on('keydown','.input-number', function (e) {
          // Allow: backspace, delete, tab, escape, enter and .
          if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
               // Allow: Ctrl+A
              (e.keyCode == 65 && e.ctrlKey === true) || 
               // Allow: home, end, left, right
              (e.keyCode >= 35 && e.keyCode <= 39)) {
                   // let it happen, don't do anything
                   return;
          }
          // Ensure that it is a number and stop the keypress
          if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
              e.preventDefault();
          }
      });


    

    // APPENDING 

    $('body').on('click','.onAppend' ,function(){
      var index = $(this).data('index');
      var html = `<div class="card mb-2">
                    <span class="remove-attr">
                    <i class="fas fa-times-circle"></i>
                    </span>
                    <div class="card-body">
                      <div class="input-group mb-3 parent"> 
                        <input type="text" class="form-control" placeholder="Attribute name"
                        name="LostAndFoundCategory[category_description][`+index+`][attribute_name]">
                        <div class="input-group-prepend">
                          <button type="button" class="input-group-text onItemAppend" id="basic-addon1" data-parent=`+index+` data-index="1"><i class="fas fa-plus"></i> Add item</button>
                        </div>
                      </div>
                      <div class="row">    

                      </div>
                    </div>
                  </div>`
                  
      $(this).attr('data-index', index + 1);
      $(this).data('index' , index +1);
      
      // console.log(index)

     $('.html').append(html)
    })

    $('body').on('click','.onItemAppend' ,function(){
      var index = $(this).data('index');
      var parent = $(this).data('parent');
      var html  = `<div class="col-md-6 mb-2">
                      <span class="remove-attr"><i class="fas fa-times-circle"></i></span>
                      <input type="text" class="form-control" placeholder="Attribute list"
                       name="LostAndFoundCategory[category_description][`+parent+`][attribute][`+index+`][attribute_list]">
                   </div>`

      $(this).attr('data-index', index + 1);
      $(this).data('index' , index +1);

      $(this).closest('.parent').next().append(html)


    });

    $('body').on('click','.remove-attr' ,function(){
        $(this).parent().remove();
    });


});

// var selectedDay   =  parseInt(startDate.format('DD') );
// var selectedMonth =  parseInt(startDate.format('MM' ) -1 );
// var selectedYear  =  parseInt(startDate.format('YYYY' ) );

// var endDateDay   =  parseInt(endDate.format('DD') );
// var endDateMonth =  parseInt(endDate.format('MM' ) -1 );
// var endDateYear  =  parseInt(endDate.format('YYYY' ) );
// //console.log(startDate.format() , endDate.format());
// //console.log(y,m);

// var data 			= {
//     selectedDay   : selectedDay,
//     selectedMonth : selectedMonth,
//     selectedYear  : selectedYear,
//     endDateDay    : endDateDay,
//     endDateMonth  : endDateMonth,
//     endDateYear   : endDateYear,
//   }